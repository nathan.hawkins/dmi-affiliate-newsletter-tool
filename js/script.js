const currentYear = new Date().getFullYear();
const htmlTemplate = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta http-equiv="imagetoolbar" content="no" />
    <title>DMi Partners | ##CLIENTNAME##</title>
    <!--[if gte mso 9]>
    <style type="text/css">
      .dividerTD {
      height: 1px !important;
      background-color: #dae1e5 !important;
      }
    </style>
    <![endif]-->
    <style type="text/css">
      /* Client-specific Styles */
      #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */
      p, h1 { font-weight: 300; margin:0; line-height:14px; }
      body{width:100% !important; background-color:#ffffff; } .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
      body{-webkit-text-size-adjust:none; margin:0 !important;} /* Prevent Webkit platforms from changing default text sizes. */
      html,body { padding:0 !important; margin:0 !important; font-family: Arial, Helvetica, sans-serif; }
      html,body,td,div,p,span,font { -webkit-text-size-adjust:none; }
      img.map, map area{ outline: none; }
      .appleLinksBlack a, span .appleLinksBlack {color: #7c858a !important; text-decoration: none !important;}
      div[style*="margin: 16px 0"] {margin:0 !important;}
      img{max-width:540px!important;height:auto;}
      @media only screen and (max-width: 485px) {
      table[class="contentMaster"], table[class="footer"], table[class="copy"], table[class="footerRight"] {
      width: 100% !important;
      }
      table[class="top"] {
      margin-bottom: 40px !important;
      }
      td.copyTD.underIMG {
      padding: 30px 20px 30px 20px !important;
      }
      td[class="nav"]{
      padding: 25px 20px 25px 20px !important;
      }
      td[class="footTD"]{
      padding: 0px 20px 6px 20px !important;
      }
      td[class="footBox"] {
      padding-bottom: 20px !important;
      }
      td[class="copyTD"] {
      padding-left: 20px !important;
      padding-right: 20px !important;
      }
      td[class="ctaTD"] {
      padding-left: 20px !important;
      padding-right: 20px !important;
      padding-top: 45px !important;
      }
      td[class="logoTD"] {
      padding-left: 0 !important;
      }
      img.assets {
      height: auto !important;
      width: 280px !important;
      }
      img.incenter {
      width: 200px !important;
      height: auto !important;
      }
      img.dmiLogo {
      margin: 0 auto !important;
      }
      .topDivider, .dividerFoot {
      width: 100% !important;
      }
      .divider {
      width: 90% !important;
      }
      }
    </style>
  </head>
  <body bgcolor="#ffffff">
    <div>
      <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;">
        <tr>
          <td align="center" bgcolor="#ffffff" style="padding:0px;">
            <table cellpadding="0" cellspacing="0" border="0" width="600" align="center" style="border-collapse:collapse;" class="contentMaster">
              <tr>
                <td bgcolor="#ffffff" align="center">
                  <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" style="border-collapse:collapse;">
                    <tr>
                      <td>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 60px;" class="top">
                          <tr>
                            <td bgcolor="#FFFFFF" align="center" class="copyTD">
                              <p style="font-size:12px; line-height:16px; color:#7c858a; font-weight:normal; text-align:center; margin-top: 20px; margin-bottom: 20px; font-family: Arial, Helvetica, sans-serif;">##HEADER-COPY##</p>
                            </td>
                          </tr>
                          <tr>
                            <td height="1" class="dividerTD">
                              <div style="background-color:#dae1e5; height:1px; width:580px; margin:0 auto; display:block;" class="topDivider"></div>
                            </td>
                          </tr>
                          <tr>
                            <td align="left" class="logoTD" style="padding-left: 12px;"><img src="https://affiliatecreatives.dmipartners.com/constants/logo.jpg" border="0" width="202" height="59" alt="DMi Partners logo" class="dmiLogo" style="display:block;"/>
                            </td>
                          </tr>
                          <tr>
                            <td height="1" class="dividerTD">
                              <div style="background-color:#dae1e5; height:1px; width:580px; margin:0 auto; display:block;" class="topDivider"></div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td align="center" style="margin-top: 60px;"><img src="https://affiliatecreatives.dmipartners.com/##FOLDER-NAME##/##LOGO-FILENAME##" class="incenter" border="0" width="##LOGOWIDTH##" height="##LOGOHEIGHT##" style="width: ##LOGOWIDTH##px !important; height: ##LOGOHEIGHT##px !important; display:block;" alt="##CLIENTNAME## logo"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td>
                  <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse;" class="copy">
                    <tr>
                      <td align="center" style="display:block; padding:40px 40px 30px 40px;" class="copyTD underIMG">
                        ##SNIPPET-TEXT##
                      </td>
                    </tr>
                    <tr>
                    <tr>
                      <td height="1" class="dividerTD">
                        <div style="background-color:#dae1e5; height:1px; width:540px; margin:0 auto; display:block;" class="divider"></div>
                      </td>
                    </tr>
                    <tr>
                      <td align="center" style="display:block; padding:25px 40px 30px 40px;" class="copyTD">
                        <h1 style="font-size:16px; line-height:24px; color:#ff6a00; font-weight:normal; text-align:left; margin-top:10px; font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Offers</h1>
                        <p style="font-size:14px; line-height:20px; color:#7c858a; font-weight:normal; text-align:left; margin-top:5px; margin-bottom: 10px; font-family: Arial, Helvetica, sans-serif;">#######OFFER CODE HERE#########</p>
                      </td>
                    </tr>
                    <tr>
                      <td height="1" class="dividerTD">
                        <div style="background-color:#dae1e5; height:1px; width:540px; margin:0 auto; display:block;" class="divider"></div>
                      </td>
                    </tr>
                    <tr>
                      <td align="center" style="display:block; padding:25px 40px 30px 40px;" class="copyTD">
                        <h1 style="font-size:16px; line-height:24px; color:#ff6a00; font-weight:normal; text-align:left; margin-top:10px; font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Banners</h1>
                        <br>#######OFFER CODE HERE#########<br>
                        <p style="font-size:14px; line-height:20px; color:#7c858a; font-weight:normal; text-align:left; margin-top:20px; margin-bottom: 10px; font-family: Arial, Helvetica, sans-serif;">Log into your account for additional banners!</p>
                      </td>
                    </tr>
                    <tr>
                      <td height="1" class="dividerTD">
                        <div style="background-color:#dae1e5; height:1px; width:540px; margin:0 auto; display:block;" class="divider"></div>
                      </td>
                    </tr>
                    <tr>
                      <td align="center" style="display:block; padding:25px 40px 30px 40px;" class="copyTD">
                        <h1 style="font-size:16px; line-height:24px; color:#ff6a00; font-weight:normal; text-align:left; margin-top:10px; font-family: Arial, Helvetica, sans-serif; font-weight: bold;">FTC Disclosures</h1>
                        <p style="font-size:14px; line-height:20px; color:#7c858a; font-weight:normal; text-align:left; margin-top:5px; margin-bottom: 10px; font-family: Arial, Helvetica, sans-serif;">All affiliates must follow compliance rules and use FTC disclosures when promoting products. You can find out more about FTC disclosures <a style="color:#ff6a00; text-decoration:none;" href="https://www.ftc.gov/tips-advice/business-center/guidance/ftcs-endorsement-guides-what-people-are-asking" target="_blank">here</a>.</p>
                      </td>
                    </tr>
                    <tr>
                      <td height="1" class="dividerTD">
                        <div style="background-color:#dae1e5; height:1px; width:540px; margin:0 auto; display:block;" class="divider"></div>
                      </td>
                    </tr>
                    <tr>
                      <td align="center" style="display:block; padding:25px 40px 30px 40px;" class="copyTD">
                        <h1 style="font-size:16px; line-height:24px; color:#ff6a00; font-weight:normal; text-align:left; margin-top:10px; font-family: Arial, Helvetica, sans-serif; font-weight: bold;">About ##CLIENTNAME##</h1>
                        ##ABOUT-CLIENT##
                      </td>
                    </tr>
                    <tr>
                      <td height="1" class="dividerTD">
                        <div style="background-color:#dae1e5; height:1px; width:540px; margin:0 auto; display:block;" class="divider"></div>
                      </td>
                    </tr>
                    <tr>
                      <td align="center" style="display:block; padding:25px 40px 30px 40px;" class="copyTD">
                        <h1 style="font-size:16px; line-height:24px; color:#ff6a00; font-weight:normal; text-align:left; margin-top:10px; font-family: Arial, Helvetica, sans-serif; font-weight: bold;">Managed by DMi Partners</h1>
                        <p style="font-size:14px; line-height:20px; color:#7c858a; font-weight:normal; text-align:left; margin-top:5px; margin-bottom: 10px; font-family: Arial, Helvetica, sans-serif;">DMi Partners is a digital marketing agency that creates award winning digital &amp; marketing solutions for our clients through our acquisition and agency services. Our affiliate management solutions help brands build a footprint in the affiliate space by providing program consulting, strategy, and execution. DMi has a proven track record of success scaling programs for our advertisers and keeping them one step ahead of their competition. Contact <a style="color:#ff6a00; text-decoration:none;" href="mailto:##CLIENT_EMAIL##">##CLIENT_EMAIL##</a> for any questions.</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td height="1" class="dividerTD">
                  <div style="background-color:#dae1e5; height:1px; width:540px; margin:0 auto; display:block;" class="dividerFoot"></div>
                </td>
              </tr>
              <tr>
                <td bgcolor="#ffffff" align="center" style="padding-bottom:35px;" class="footBox">
                  <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" style="border-collapse:collapse;">
                    <tr>
                      <td >
                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="border-collapse:collapse;" class="footerRight">
                          <tr>
                            <td style="padding:25px 0 15px 0;" class="socialBlock" align="center">
                              <img src="https://affiliatecreatives.dmipartners.com/constants/dmi-social.jpg" width="94" height="16" alt="social media buttons" usemap="#logos" class="map" border="0" />
                              <map name="logos" id="logos">
                                <area shape="rect" coords="0,0,16,16" href="https://www.facebook.com/dmipartners" title="Facebook" alt="Facebook" target="_blank" />
                                <area shape="rect" coords="26,0,41,16" href="https://twitter.com/DMi_Partners" title="Twitter" alt="Twitter" target="_blank" />
                                <area shape="rect" coords="52,0,68,16" href="https://www.instagram.com/dmi_partners/" title="Instagram" alt="Instagram" target="_blank" />
                                <area shape="rect" coords="78,0,93,16" href="https://www.linkedin.com/company/dmi-partners/" title="Linkedin" alt="Linkedin" target="_blank" />
                              </map>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                  <table cellpadding="0" cellspacing="0" border="0" width="600" align="center" style="border-collapse:collapse;" class="footer">
                    <tr>
                      <td align="center" style="font-family: Arial, Helvetica, sans-serif; padding:0 80px 0 80px;" class="footTD">
                        <p style="font-size:12px; line-height:15px; color:#7c858a; font-weight:normal; text-align:center; margin-top: 10px; margin-bottom: 10px">©${currentYear} DMi Partners</p>
                        <p style="font-size:12px; line-height:15px; color:#7c858a; font-weight:normal; text-align:center;  margin-top: 10px; margin-bottom: 10px;"><span class="appleLinksBlack">1 S Broad St.</span><span style="color:#7c858a;" class="hide"> &#9679; </span><span class="appleLinksBlack">Philadelphia, PA 19107</span></p>
                        <p style="font-size:12px; line-height:15px; color:#7c858a; font-weight:normal; text-align:center; margin-top: 10px; margin-bottom: 10px">To ensure delivery of these emails to your inbox, please add <span class="appleLinksBlack">##CLIENT_EMAIL##</span> to your address book or list of safe senders.</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>`;
// Function for 'Generate & Download'
document.getElementById('generateBtn').addEventListener('click', () => {
    const newHtml = generateHtml();
    downloadHtml(newHtml);
    document.getElementById('codeBlock').textContent = newHtml;
});

// Function for 'Generate Only'
document.getElementById('generateOnly').addEventListener('click', () => {
    const newHtml = generateHtml();
    document.getElementById('codeBlock').textContent = newHtml;
});

function generateHtml() {
    let newHtml = htmlTemplate;
    const clientName = document.getElementById('clientName').value; // Get the client name value

    const clientNameRegex = /##CLIENTNAME##/g;
    const logoWidthRegex = /##LOGOWIDTH##/g;
    const logoHeightRegex = /##LOGOHEIGHT##/g;

    newHtml = newHtml.replace(clientNameRegex, clientName);
    newHtml = newHtml.replace('##HEADER-COPY##', document.getElementById('headerCopy').value);
    newHtml = newHtml.replace('##FOLDER-NAME##', document.getElementById('folderName').value);
    newHtml = newHtml.replace('##LOGO-FILENAME##', document.getElementById('logoFilename').value);
    newHtml = newHtml.replace(logoWidthRegex, document.getElementById('logoWidth').value);
    newHtml = newHtml.replace(logoHeightRegex, document.getElementById('logoHeight').value);

    const wrapParagraphs = text => {
        return text.split('\n').map(line => `<p style="font-size:14px; line-height:20px; color:#7c858a; font-weight:normal; text-align:left; margin-top: 10px; margin-bottom:10px; font-family: Arial, Helvetica, sans-serif;">${line}</p>`).join('');
    };

    let snippetText = document.getElementById('snippetText').value;
    const emailRegex = /[\w.-]+@[\w.-]+\.\w+/;
    const foundEmail = snippetText.match(emailRegex);
    // Use the client name in the default email address
    let emailForReplacement = `${clientName.toLowerCase().replace(/\s+/g, '')}@dmipartners.com`;

    if (foundEmail) {
        snippetText = snippetText.replace(foundEmail[0], `<a style="color:#ff6a00; text-decoration:none;" href="mailto:${foundEmail[0]}">${foundEmail[0]}</a>`);
        emailForReplacement = foundEmail[0];
    }

    newHtml = newHtml.replace('##SNIPPET-TEXT##', wrapParagraphs(snippetText));
    newHtml = newHtml.replace('##ABOUT-CLIENT##', wrapParagraphs(document.getElementById('aboutClient').value));
    // Replace the placeholder with the email, whether found or default
    newHtml = newHtml.replaceAll('##CLIENT_EMAIL##', emailForReplacement);

    return newHtml;
}
function downloadHtml(htmlContent) {
    const folderName = document.getElementById('folderName').value;
    const filename = folderName + '.html';
    const blob = new Blob([htmlContent], { type: 'text/html' });

    const downloadLink = document.createElement('a');
    downloadLink.href = URL.createObjectURL(blob);
    downloadLink.download = filename;

    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
}

document.getElementById('copyBtn').addEventListener('click', () => {
    const code = document.getElementById('codeBlock').textContent;
    navigator.clipboard.writeText(code).then(() => {
        alert('Code copied to clipboard!');
    });
});

function adjustTextareaHeight(textarea) {
    textarea.style.height = 'auto';
    const newHeight = textarea.scrollHeight;
    textarea.style.height = newHeight > 80 ? newHeight + 'px' : '80px';
}

document.querySelectorAll('.container textarea').forEach(textarea => {
    textarea.addEventListener('input', function() {
        adjustTextareaHeight(this);
    });

    adjustTextareaHeight(textarea);
});

